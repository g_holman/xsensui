% clear all;
function [ellipseFit] = PlaneRelationship(data,crop)

% Create data

% data = [headGyroFiltered(:,1) headGyroFiltered(:,2) headGyroFiltered(:,3)];
combos = [2 3];

if crop == true
    data = data(1:120*128,:);
end

for i = 1
% Calculate the eigenvectors and eigenvalues
covariance = cov(data(:,combos(i,:)));
[eigenvec, eigenval ] = eig(covariance);

% Get the index of the largest eigenvector
[largest_eigenvec_ind_c, r] = find(eigenval == max(max(eigenval)));
largest_eigenvec = eigenvec(:, largest_eigenvec_ind_c);

% Get the largest eigenvalue
largest_eigenval = max(max(eigenval));

% Get the smallest eigenvector and eigenvalue
if(largest_eigenvec_ind_c == 1)
    smallest_eigenval = max(eigenval(:,2));
    smallest_eigenvec = eigenvec(:,2);
else
    smallest_eigenval = max(eigenval(:,1));
    smallest_eigenvec = eigenvec(1,:);
end

% Calculate the angle between the x-axis and the largest eigenvector
angle = atan2(largest_eigenvec(2), largest_eigenvec(1));

% This angle is between -pi and pi.
% Let's shift it such that the angle is between 0 and 2pi
if(angle < 0)
    angle = angle + 2*pi;
end

% Get the coordinates of the data mean
avg = mean(data(:,combos(i,:)));

% Get the 95% confidence interval error ellipse
chisquare_val = 2.4477; % 90% = 2.1459, 95% = 2.4477, 99% = 3.0348
theta_grid = linspace(0,2*pi);
phi = angle;
X0=avg(1);
Y0=avg(2);
a=chisquare_val*sqrt(largest_eigenval);
b=chisquare_val*sqrt(smallest_eigenval);

% the ellipse in x and y coordinates 
ellipse_x_r  = a*cos( theta_grid );
ellipse_y_r  = b*sin( theta_grid );

%Define a rotation matrix
R = [ cos(phi) sin(phi); -sin(phi) cos(phi) ];

%let's rotate the ellipse to some angle phi
r_ellipse = [ellipse_x_r;ellipse_y_r]' * R;

% Draw the error ellipse
% subplot(1,3,i)

hold on;

% Plot the original data
plot(data(:,combos(i,1)), data(:,combos(i,2)), '.');
mindata = min(min(data(:,combos(i,:))));
maxdata = max(max(data(:,combos(i,:))));
xlim([mindata-3, maxdata+3]);
xlim([mindata-3, maxdata+3]);
axis equal
hold on;

plot(r_ellipse(:,1) + X0,r_ellipse(:,2) + Y0,'-')

% Plot the eigenvectors
quiver(X0, Y0, largest_eigenvec(1)*sqrt(largest_eigenval), largest_eigenvec(2)*sqrt(largest_eigenval), '-m', 'LineWidth',2);
quiver(X0, Y0, smallest_eigenvec(1)*sqrt(smallest_eigenval), smallest_eigenvec(2)*sqrt(smallest_eigenval), '-g', 'LineWidth',2);
axis equal
hold on;

% Set the axis labels
hXLabel = xlabel('Y_a_c_c - ML Plane');
hYLabel = ylabel('Z_a_c_c - AP Plane');
% title('1=X 2=Y 3=Z')

ellipseFit.angle(i,:) = angle*180/pi();
ellipseFit.MinorAxis(i,:) = b*2;
ellipseFit.MajorAxis(i,:) = a*2;
ellipseFit.MinorAxisVec(i,:) = largest_eigenvec.';
ellipseFit.MajorAxisVec(i,:) = smallest_eigenvec.';
ellipseFit.Area(i,:) = pi()*a*b;
end
end
