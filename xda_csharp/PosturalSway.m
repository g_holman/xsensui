function pathlength = PosturalSway(filename, exportName)

% close all; clear all;
% filename = 'fiveSecondsWMove_03.mtb';
% exportName = 'test';
[~, count, Data] = mainFileLoader(filename);
%%
for i = 1:length(Data)
    acc.x(i) = Data(i).acc(1);
    acc.y(i) = Data(i).acc(2);
    acc.z(i) = Data(i).acc(3);
    gyro.x(i) = Data(i).gyro(1);
    gyro.y(i) = Data(i).gyro(2);
    gyro.z(i) = Data(i).gyro(3);
end

% avgAccX = mean(acc.x);
% avgAccY = mean(acc.y);

freq = 62;
count = count - count(1);
time = count/freq;

% hold on
% plot(time,acc.x)
% plot(time,acc.y)
% hold off

%% write to txt file
T = table(acc.x.',acc.y.',gyro.x.',gyro.y.',gyro.z.','VariableNames',{'AccX','AccY','GyroX','GyroY','GyroZ'});
writetable(T,strcat(exportName,'.txt'));

%% Determine Path Length
five_sec = find(time == 5);

pathlength.x = sum(diff(acc.x(five_sec:end))) / max(time);
pathlength.y = sum(diff(acc.y(five_sec:end))) / max(time);
pathlength.z = sum(diff(acc.z(five_sec:end))) / max(time);

path_length = table(pathlength.x, pathlength.y, pathlength.z,'VariableNames',{'ps_x','ps_y', 'ps_z'});
writetable(path_length,strcat(extractBefore(filename,'.mtb'),'.txt'));

% Path_Header = {'x','y','z'};
% xlswrite(strcat(exportName,'_Pathlength','.xlsx'),Path_Header,1,'A1');
% xlswrite(strcat(exportName,'_Pathlength','.xlsx'),{pathlength.x,pathlength.y,pathlength.z},1,'A2');

%% Determine Ellipse Fit
headGyroFiltered = [acc.x(five_sec:end).' acc.y(five_sec:end).' acc.z(five_sec:end).'];
crop = 'false';

% [ellipseFit3D.Head] = PlaneRelationship3D(headGyroFiltered,crop);
[ellipseFit2D.Head] = PlaneRelationship(headGyroFiltered,crop);

% ellipseExport3D(ellipseFit3D,exportName,1);
ellipseExport2D(ellipseFit2D,exportName,2);



end