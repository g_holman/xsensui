function ellipseExport3D(ellipseFit3D,writename,sheet)
%%
Ellipse_Header3D = {'Head','Angle-Yaw,Pitch,Roll (deg)','Major Axis (deg/s)','Minor Axis (deg/s)','Middle Axis (deg/s)','Volume (deg/s)^2'};%,'Major Vector','Minor Vector'};
if isfield(ellipseFit3D,'Head') == 1
xlswrite(strcat(writename,'_Ellipse','.xlsx'),{'Head'},sheet,'A1');
xlswrite(strcat(writename,'_Ellipse','.xlsx'),Ellipse_Header3D,sheet,'A1');
xlswrite(strcat(writename,'_Ellipse','.xlsx'),ellipseFit3D.Head.anglex,sheet,'B2');
xlswrite(strcat(writename,'_Ellipse','.xlsx'),ellipseFit3D.Head.angley,sheet,'B3');
xlswrite(strcat(writename,'_Ellipse','.xlsx'),ellipseFit3D.Head.anglez,sheet,'B4');
xlswrite(strcat(writename,'_Ellipse','.xlsx'),ellipseFit3D.Head.MajorAxis,sheet,'C2');
xlswrite(strcat(writename,'_Ellipse','.xlsx'),ellipseFit3D.Head.MinorAxis,sheet,'D2');
xlswrite(strcat(writename,'_Ellipse','.xlsx'),ellipseFit3D.Head.MiddleAxis,sheet,'E2');
xlswrite(strcat(writename,'_Ellipse','.xlsx'),ellipseFit3D.Head.Volume,1,'F2');
end
if isfield(ellipseFit3D,'Strn') == 1
xlswrite(strcat(writename,'_Ellipse','.xlsx'),Ellipse_Header3D,sheet,'A6');
xlswrite(strcat(writename,'_Ellipse','.xlsx'),{'Sternum'},1,'A6');
xlswrite(strcat(writename,'_Ellipse','.xlsx'),ellipseFit3D.Strn.anglex,sheet,'B7');
xlswrite(strcat(writename,'_Ellipse','.xlsx'),ellipseFit3D.Strn.angley,sheet,'B8');
xlswrite(strcat(writename,'_Ellipse','.xlsx'),ellipseFit3D.Strn.anglez,sheet,'B9');
xlswrite(strcat(writename,'_Ellipse','.xlsx'),ellipseFit3D.Strn.MajorAxis,sheet,'C7');
xlswrite(strcat(writename,'_Ellipse','.xlsx'),ellipseFit3D.Strn.MinorAxis,sheet,'D7');
xlswrite(strcat(writename,'_Ellipse','.xlsx'),ellipseFit3D.Strn.MiddleAxis,sheet,'E7');
xlswrite(strcat(writename,'_Ellipse','.xlsx'),ellipseFit3D.Strn.Volume,sheet,'F7');
end
%%
end