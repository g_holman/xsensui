/*	Copyright (c) 2003-2016 Xsens Technologies B.V. or subsidiaries worldwide.
	All rights reserved.

	Redistribution and use in source and binary forms, with or without modification,
	are permitted provided that the following conditions are met:

	1.	Redistributions of source code must retain the above copyright notice,
		this list of conditions and the following disclaimer.

	2.	Redistributions in binary form must reproduce the above copyright notice,
		this list of conditions and the following disclaimer in the documentation
		and/or other materials provided with the distribution.

	3.	Neither the names of the copyright holders nor the names of their contributors
		may be used to endorse or promote products derived from this software without
		specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
	THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
	OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
	HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY OR
	TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Xsens;
using XDA;
using Microsoft.VisualBasic.FileIO;
using System.Threading.Tasks;
using System.Diagnostics;

namespace MTwExample
{
    public partial class Form1 : Form
    {
        private MyXda _xda;
		private XsDevice _measuringDevice = null;
		private Dictionary<XsDevice, MyMtCallback> _measuringMts = new Dictionary<XsDevice, MyMtCallback>();
		private Dictionary<uint, ConnectedMtData> _connectedMtwData = new Dictionary<uint, ConnectedMtData>();
		private List<string> _availableFileNames = new List<string>();
		private int timeToRecordSeconds = 0;
		private bool isUnlimitedTime = true;
		private int elapsedTimeMs = 0;
		private MLApp.MLApp _matlabServer;

        public Form1()
        {
            InitializeComponent();
			var mltype = Type.GetTypeFromProgID("Matlab.Application");
			_matlabServer = (MLApp.MLApp)Activator.CreateInstance(mltype);
            _xda = new MyXda();
			cbxChannel.SelectedIndex = 0;
            step(1);
			GetFileNames();
			timeSelectBox.SelectedIndex = 6;
			isUnlimitedTime = true;
        }

		private void Form1_FormClosed(object sender, FormClosedEventArgs e)
		{
			liveMeasurementTimer.Enabled = false;

			if (_measuringDevice != null)
				_measuringDevice.clearCallbackHandlers();
			
			_measuringMts.Clear();

			_xda.Dispose();
			_xda = null;
		}

		private void recordButton_Click(object sender, EventArgs e)
		{
			Console.WriteLine("Beginning recording");

			setNewLogFileName();
			recordButton.Enabled = false;
			stopRecordingButton.Enabled = true;

            recordingStatusBox.Text = $"Recording for {timeSelectBox.Text} seconds";

			_measuringDevice.startRecording();

			// TODO: Add timer measurement thing
			elapsedTimeMs = 0;
			recordingTimer.Interval = 20;
			recordingTimer.Start();
		}

		private void setNewLogFileName()
		{
			var number = sampleIdNumberBox.Value <= 9 ? "0" + sampleIdNumberBox.Value : sampleIdNumberBox.Value.ToString();
			_measuringDevice.createLogFile(new XsString(sampleNameTextBox.Text + "_" + number + ".mtb"));
		}

		private void stopRecordingButton_Click(object sender, EventArgs e)
        {
			stopRecording();
        }

		private void stopRecording()
        {
            recordingStatusBox.Text = $"Recorded for {elapsedTimeMs} milliseconds";
			recordingTimer.Stop();

			stopRecordingButton.Enabled = false;
            liveMeasurementTimer.Enabled = false;

            if (_measuringDevice.isRecording())
				_measuringDevice.stopRecording();


			Task.Delay(250); // Give it time to wrap up recording

			_measuringDevice.closeLogFile();


			Task.Delay(250); // Give it time to close the file

			recordButton.Enabled = true;

			GetFileNames();

        }

        private void stopAllButton_Click(object sender, EventArgs e)
        {
			// button3.Enabled = false;
			liveMeasurementTimer.Enabled = false;
			recordingTimer.Stop();

			if (_measuringDevice.isRecording())
				_measuringDevice.stopRecording();
            _measuringDevice.gotoConfig();
            _measuringDevice.disableRadio();
            _measuringDevice.clearCallbackHandlers();

            btnScan.Enabled = true;

            if (cbxStations.Items.Count > 0)
            {
                cbxStations.SelectedIndex = 0;
                btnEnable.Enabled = true;
                step(2);
            }
            else
            {
                step(1);
            }
        }

        private void recordingTimer_Tick_1(object sender, EventArgs e)
        {
            elapsedTimeMs += recordingTimer.Interval;

			if (!isUnlimitedTime)
            {
                if (elapsedTimeMs >= timeToRecordSeconds * 1000)
                {
                    stopRecording();
                }
            }
        }

		private void recordingTimer_Tick(object sender, EventArgs e)
        {
			string text = "";
			foreach (KeyValuePair<uint, ConnectedMtData> data in _connectedMtwData)
			{
				if (data.Value._orientation != null)
				{
					var accData = data.Value._calibratedData.m_acc;
					text += string.Format("{3:X8} {0,-5:f2}, {1,-5:f2}, {2,-5:f2} {4}[°]\n",
										   accData.value(0),
										   accData.value(1),
										   accData.value(2),
										   data.Key,
										   data.Value._sumFrameSkips);
				}
			}
			rtbData.Text = text;
		}

        private void btnScan_Click(object sender, EventArgs e)
        {
            cbxStations.Items.Clear();
			_xda.scanPorts();
			if (_xda._DetectedDevices.Count > 0)
			{
				foreach (XsPortInfo portInfo in _xda._DetectedDevices)
				{
					if (portInfo.deviceId().isWirelessMaster())
					{
						_xda.openPort(portInfo);
						MasterInfo ai = new MasterInfo(portInfo.deviceId());
						ai.ComPort = portInfo.portName();
						ai.BaudRate = portInfo.baudrate();
						cbxStations.Items.Add(ai);
						break;
					}
				}

				if (cbxStations.Items.Count > 0)
				{
					cbxStations.SelectedIndex = 0;
					btnEnable.Enabled = true;
					step(2);
				}
				else
				{
					step(3);
				}
			}
        }

        private void btnEnable_Click(object sender, EventArgs e)
        {
			XsDevice device = _xda.getDevice(((MasterInfo)cbxStations.SelectedItem).DeviceId);
			if (device.isRadioEnabled())
				device.disableRadio();
            if (device.enableRadio(Convert.ToInt32(cbxChannel.Text)))
            {
                btnScan.Enabled = false;
                btnEnable.Enabled = false;
                btnMeasure.Enabled = true;
                step(4);
            }
            else
            {
                step(6);
                btnScan.Enabled = true;
                btnEnable.Enabled = false;
            }
        }

        private void btnMeasure_Click(object sender, EventArgs e)
        {
            step(9);
			_connectedMtwData.Clear();
			_measuringDevice = _xda.getDevice(((MasterInfo)cbxStations.SelectedItem).DeviceId);
			_measuringDevice.gotoMeasurement();
			XsDevicePtrArray deviceIds = _measuringDevice.children();
			for (uint i = 0; i < deviceIds.size(); i++)
			{
				XsDevice mtw = new XsDevice(deviceIds.at(i));
				MyMtCallback callback = new MyMtCallback();

				ConnectedMtData mtwData = new ConnectedMtData();
				_connectedMtwData.Add(mtw.deviceId().toInt(), mtwData);

				// connect signals
				callback.DataAvailable += new EventHandler<DataAvailableArgs>(_callbackHandler_DataAvailable);

				mtw.addCallbackHandler(callback);
				_measuringMts.Add(mtw, callback);
			}
			lblDeviceCount.Text = string.Format("MTw's Connected: {0}", deviceIds.size());
            btnMeasure.Enabled = false;
			recordButton.Enabled = true;
            stopRecordingButton.Enabled = true;
			stopAllButton.Enabled = true;
            liveMeasurementTimer.Interval = 20;
            liveMeasurementTimer.Enabled = true;
        }

        private void liveMeasurementTimer_Tick(object sender, EventArgs e)
        {
			string text = "";
		    foreach (KeyValuePair<uint, ConnectedMtData> data in _connectedMtwData)
            {
				
				if (data.Value._orientation != null)
                {
					text += string.Format("Ori X: {0,-5:f2}\nOri Y: {1,-5:f2}\nOri Z: {2,-5:f2}\n {3:X8}[°]\n",
										   data.Value._orientation.x(),
										   data.Value._orientation.y(),
										   data.Value._orientation.z(),
										   data.Key);
                }
				if (data.Value._calibratedData != null)
				{
					var calibratedData = data.Value._calibratedData?.m_acc;
					text += string.Format("Acc X: {0,-5:f2}\nAcc Y: {1,-5:f2}\nAcc Z: {2,-5:f2}\nFrame Skips: {4}[°]\n",
										   calibratedData.value(0),
										   calibratedData.value(1),
										   calibratedData.value(2),
										   data.Key,
										   data.Value._sumFrameSkips);

				}
				else
                {
					text += "No acceleration data available";
                }
			}
			rtbData.Text = text;
        }

		void _callbackHandler_DataAvailable(object sender, DataAvailableArgs e)
		{
			if (InvokeRequired)
			{
				// Update UI, make sure this happens on the UI thread
				BeginInvoke(new Action(delegate { _callbackHandler_DataAvailable(sender, e); }));
			}
			else
			{
				//Getting Euler angles.
				XsEuler oriEuler = e.Packet.orientationEuler();

				_connectedMtwData[e.Device.deviceId().toInt()]._orientation = oriEuler;
			}
		}

        /// <summary>
        /// Some step by step info
        /// </summary>
        /// <param name="stepNumber"></param>
        private void step(int stepNumber)
        {
            switch (stepNumber)
            {
                case 1:
                    rtbSteps.Text = "Connect the Awinda Station, dock the MTWs and click 'Scan'.";
                    break;
                case 2:
                    rtbSteps.Text = "Select a station from the combobox, select a channel and click 'Enable Radio'.";
                    break;
                case 3:
                    rtbSteps.Text = "No station was found, make sure the drivers were installed correctly.";
                    break;
                case 4:
                    rtbSteps.Text = "Undock the MTws from the station and wait for them to connect to the station. This is indicated by the MTw LEDs blinking in sync with the CONN LED on the station.\n\n" +
                                    "Then click 'Measure' and wait a bit.";
                    break;
                case 6:
                    rtbSteps.Text = "There was an unexpected failure enabling the station.";
                    break;
                case 8:
                    rtbSteps.Text = "There was an unexpected failure going to the operational mode.";
                    break;
                case 9:
                    rtbSteps.Text = "Enjoy the data... click 'Stop' when no further joy can be obtained from looking at the data.\nClick 'Record' when you want to feel the same joy again at a later moment";
                    break;
                default:
                    rtbSteps.Text = "No station was found, make sure the drivers were installed correctly.";
                    break;
            }
        }

		private void RunMatlabAnalysisOnFile(string fileName)
        {
			Console.WriteLine("Running Matlab Analysis on " + fileName);
			analysisProgressLabel.Text = "Analyzing " + fileName;
			matlabProgressBar.Value = 10;

			try
            {
                object result;

			    _matlabServer.Execute("cd " + FileSystem.CurrentDirectory);

				_matlabServer.Feval("PosturalSway", 1, out result, fileName, fileName.Split('.')[0] + "_Analysis");

				Console.WriteLine(result);

				GetFileNames();

            }
			catch (Exception e)
            {
				Console.WriteLine(e);
				Console.WriteLine("Could not run Matlab Analysis");
            }
			finally
            {
				analysisProgressLabel.Text = "Completed analysis on " + fileName;
				matlabProgressBar.Value = 100;
			}
		}

        private void executeMatlabScriptButton_Click(object sender, EventArgs e)
        {
            //FileList.Text += "------------------------------\n";
            //try
            //{
            //	FileList.Text += "is init" +_measuringDevice?.isInitialized() + "\n";
            //	FileList.Text += "is motion track " + _measuringDevice?.isMotionTracker() + "\n";
            //	FileList.Text += "device state " + _measuringDevice?.deviceState().ToString() + "\n";
            //	FileList.Text += "device state " + _measuringDevice?.deviceMode() + "\n";
            //	FileList.Text += "calibrated state " + _measuringDevice?.deviceMode().calibratedDataMode() + "\n";
            //	FileList.Text += "accelerometer" + _measuringDevice?.accelerometerRange() + "\n";
            //	var deviceMode = _measuringDevice.deviceMode();
            //	deviceMode.setCalibratedDataMode(XsCalibratedDataMode.XCDM_Acceleration);
            //	_measuringDevice.deviceMode().setCalibratedDataMode(XsCalibratedDataMode.XCDM_AccGyr);
            //	FileList.Text += "device set " + _measuringDevice.deviceMode() + "\n";
            //	_measuringDevice.accessControlMode();
            //}
            //catch { FileList.Text += "No device connected"; }
        }

        private void refreshFileNamesButton_Click(object sender, EventArgs e)
        {
			GetFileNames();
		}

		private void GetFileNames()
        {
			var currentDirectory = FileSystem.CurrentDirectory;

			mtbFilesTable.Rows.Clear();

			var newAvailableFileNamesList = new List<string>();
			var fileNames = FileSystem.GetFiles(FileSystem.CurrentDirectory);

			foreach(var fileName in fileNames)
            {
                // TODO: Make them like clickable list items or something
                if (fileName.EndsWith(".mtb"))
				{
					Console.WriteLine(fileName);
					var extractedFileName = FileSystem.GetFileInfo(fileName).Name;
					var filePrefix = FileSystem.GetName(extractedFileName).Split('.')[0];

					newAvailableFileNamesList.Add(extractedFileName);

					var isMtbFileAnalyzed = FileSystem.FileExists(FileSystem.GetName(extractedFileName).Split('.')[0] + "_Analysis.txt");

					var newMtbFile = new MTBFile(FileSystem.GetName(extractedFileName), 
						FileSystem.GetFileInfo(fileName).Length.ToString(), 
						isMtbFileAnalyzed);

					DataGridViewRow newRow = (DataGridViewRow)mtbFilesTable.Rows[0].Clone();

					newRow.Cells[0].Value = newMtbFile.FileName;
					newRow.Cells[1].Value = newMtbFile.FileSize;
					newRow.Cells[2].Value = "Run Matlab Analysis";
					newRow.Cells[3].Value = newMtbFile.IsAnalyzed;

					if (FileSystem.FileExists(filePrefix + ".txt"))
                    {
						var posturalSwayData = FileSystem.ReadAllText(filePrefix + ".txt");
						Console.WriteLine(posturalSwayData);
						var varRow = posturalSwayData.Split('\n')[1];
						var variables = varRow.Split(',');
						newRow.Cells[4].Value = variables[0];
						newRow.Cells[5].Value = variables[1];
						newRow.Cells[6].Value = variables[2];
                    }

					mtbFilesTable.Rows.Add(newRow);
				}
			}

			_availableFileNames = newAvailableFileNamesList;
			UpdateSampleNameAndNumber();
        }

		private void UpdateSampleNameAndNumber()
        {
			var maxInt = 0;
			_availableFileNames.ForEach(fileName =>
			{
				Console.WriteLine(fileName);
				if (fileName.StartsWith(sampleNameTextBox.Text))
				{
					try
                    {
                        var splitFileName = fileName.Split('_');
						var number = int.Parse(splitFileName[1].Split('.')[0]);
						Console.WriteLine(number);
						Console.WriteLine(maxInt);
						if (number > maxInt)
                        {
							maxInt = number;
						}

						sampleIdNumberBox.Value = maxInt + 1;
					}
					catch (Exception e)
                    {
						Console.WriteLine(e);
                    }
				}
			});
        }

        private void isAutoIncremented_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void timeSelectBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool success = Int32.TryParse(timeSelectBox.Text, out int number);
            if (success)
			{
				isUnlimitedTime = false;
				timeToRecordSeconds = number;
			}
			else
			{
				timeToRecordSeconds = 0;
				isUnlimitedTime = true;
			}
		}

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

		// Open Containing Folder Button
        private void button10_Click(object sender, EventArgs e)
        {
			Process.Start("explorer.exe", FileSystem.CurrentDirectory);
		}

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
			if (e.ColumnIndex == 2)
            {
				var fileName = mtbFilesTable.Rows[e.RowIndex].Cells[0].Value.ToString();
				RunMatlabAnalysisOnFile(fileName);
            }
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void rtbSteps_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxFilename_TextChanged(object sender, EventArgs e)
        {
			// Change Variable
        }

        public class MTBFile
        {
            public string FileName;
            public string FileSize;
            public bool IsAnalyzed;

			public void OnClick()
            {
				Console.WriteLine("Analyzing " + FileName);
            }
            // Potentially one more for the button on click?
            
            public MTBFile(string fileName, string fileSize, bool isAnalyzed)
            {
                FileName = fileName;
                FileSize = fileSize;
                IsAnalyzed = isAnalyzed;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
