function ellipseExport2D(ellipseFit2D,writename,sheet)
Ellipse_Header2D = {'','Angle(deg)','Area (deg/s)^2','Major Axis (deg/s)','Minor Axis (deg/s)'};
if isfield(ellipseFit2D,'Head') == 1
xlswrite(strcat(writename,'_Ellipse','.xlsx'),Ellipse_Header2D,sheet,'A1');
xlswrite(strcat(writename,'_Ellipse','.xlsx'),{'Head';'Pitch-Yaw';'Roll-Yaw';'Pitch-Roll'},2,'A1');
xlswrite(strcat(writename,'_Ellipse','.xlsx'),ellipseFit2D.Head.angle,sheet,'B2');
xlswrite(strcat(writename,'_Ellipse','.xlsx'),ellipseFit2D.Head.Area,sheet,'C2');
xlswrite(strcat(writename,'_Ellipse','.xlsx'),ellipseFit2D.Head.MajorAxis,sheet,'D2');
xlswrite(strcat(writename,'_Ellipse','.xlsx'),ellipseFit2D.Head.MinorAxis,sheet,'E2');
end
if isfield(ellipseFit2D,'Strn') == 1
xlswrite(strcat(writename,'_Ellipse','.xlsx'),Ellipse_Header2D,sheet,'A6');
xlswrite(strcat(writename,'_Ellipse','.xlsx'),{'Strn';'Pitch-Yaw';'Roll-Yaw';'Pitch-Roll'},2,'A6');
xlswrite(strcat(writename,'_Ellipse','.xlsx'),ellipseFit2D.Strn.angle,sheet,'B7');
xlswrite(strcat(writename,'_Ellipse','.xlsx'),ellipseFit2D.Strn.Area,sheet,'C7');
xlswrite(strcat(writename,'_Ellipse','.xlsx'),ellipseFit2D.Strn.MajorAxis,sheet,'D7');
xlswrite(strcat(writename,'_Ellipse','.xlsx'),ellipseFit2D.Strn.MinorAxis,sheet,'E7');
end
end