% clear all;
function [ellipseFit3D] = PlaneRelationship3D(data,crop)

% Crop data to first two minutes, comment out for whole trial
if crop == true
    data = data(1:120*128,:);
end
%%
% Calculate the eigenvectors and eigenvalues
covariance = cov(data);
[eigenvec, eigenval ] = eig(covariance);

% Get the index of the largest eigenvector
[largest_eigenvec_ind_c,~] = find(eigenval == max(max(eigenval)));
largest_eigenvec = eigenvec(:, largest_eigenvec_ind_c);

% Get the largest eigenvalue
largest_eigenval = max(max(eigenval));

% Get the index of the samllest eigenvector
[smallest_eigenvec_ind_c,~] = find(eigenval == min(eigenval(eigenval>0)));
smallest_eigenvec = eigenvec(:, smallest_eigenvec_ind_c);

% Get the smallest eigenvalue
smallest_eigenval = min(eigenval(eigenval>0));


r = setdiff([1:3],[smallest_eigenvec_ind_c largest_eigenvec_ind_c]);
% Get the index of the middle eigenvector
middle_eigenvec = eigenvec(:, r);

% Get the middle eigenvalue
middle_eigenval = max(eigenval(:,r));

% Define axes
axisdef = [1 0 0 ;0 1 0 ; 0 0 1];
major_axis = largest_eigenvec*sqrt(largest_eigenval);
% minor_axis = [smallest_eigenvec*sqrt(smallest_eigenval)];
% middle_axis = [middle_eigenvec*sqrt(middle_eigenval)];


% Calculate the angle between the x-axis and the largest eigenvector

angle_x = acosd(dot(axisdef(:,1),major_axis)/norm(major_axis));
angle_y = acosd(dot(axisdef(:,2),major_axis)/norm(major_axis));
angle_z = acosd(dot(axisdef(:,3),major_axis)/norm(major_axis));

angletot = angle_x + angle_y + angle_z;

% Get the coordinates of the data mean
avg = mean(data(:,1:3));

% Get the 95% confidence interval error ellipse
chisquare_val = 2.4477; % 90% = 2.1459, 95% = 2.4477, 99% = 3.0348
X0=avg(largest_eigenvec_ind_c);
Y0=avg(r);
Z0=avg(smallest_eigenvec_ind_c);
a=chisquare_val*sqrt(largest_eigenval);
b=chisquare_val*sqrt(middle_eigenval);
c=chisquare_val*sqrt(smallest_eigenval);

%%
figure;
scatter3(data(:,1), data(:,2),data(:,3),'.');
hold on
xlabel 'Yaw'
ylabel 'Pitch';
zlabel 'Roll';
title('Planar Relationship')
axis equal

[x y z] = ellipsoid(X0, Y0, Z0, a, b, c);
h = surfl(x, y, z); 
set(h, 'FaceAlpha', 0.5)
shading interp
axis equal

% Plot the eigenvectors
quiver3(X0, Y0, Z0, largest_eigenvec(1)*sqrt(largest_eigenval), largest_eigenvec(2)*sqrt(largest_eigenval), largest_eigenvec(3)*sqrt(largest_eigenval),'-m', 'LineWidth',2);
quiver3(X0, Y0, Z0, smallest_eigenvec(1)*sqrt(smallest_eigenval), smallest_eigenvec(2)*sqrt(smallest_eigenval), smallest_eigenvec(3)*sqrt(smallest_eigenval),'-g', 'LineWidth',2);
quiver3(X0, Y0, Z0, middle_eigenvec(1)*sqrt(middle_eigenval), middle_eigenvec(2)*sqrt(middle_eigenval),middle_eigenvec(3)*sqrt(middle_eigenval), '-p', 'LineWidth',2);

% Set the axis labels

hold off
%%
ellipseFit3D.anglex = angle_x;
ellipseFit3D.angley = angle_y;
ellipseFit3D.anglez = angle_z;
ellipseFit3D.centroid = [X0 Y0 Z0];
ellipseFit3D.MinorAxis = b*2;
ellipseFit3D.MajorAxis = a*2;
ellipseFit3D.MiddleAxis = c*2;
ellipseFit3D.Volume = (4/3)*pi()*a*b*c;


end
