/*	Copyright (c) 2003-2016 Xsens Technologies B.V. or subsidiaries worldwide.
	All rights reserved.

	Redistribution and use in source and binary forms, with or without modification,
	are permitted provided that the following conditions are met:

	1.	Redistributions of source code must retain the above copyright notice,
		this list of conditions and the following disclaimer.

	2.	Redistributions in binary form must reproduce the above copyright notice,
		this list of conditions and the following disclaimer in the documentation
		and/or other materials provided with the distribution.

	3.	Neither the names of the copyright holders nor the names of their contributors
		may be used to endorse or promote products derived from this software without
		specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
	THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
	OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
	HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY OR
	TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

﻿namespace MTwExample
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnScan = new System.Windows.Forms.Button();
            this.btnEnable = new System.Windows.Forms.Button();
            this.lblChannel = new System.Windows.Forms.Label();
            this.cbxStations = new System.Windows.Forms.ComboBox();
            this.lblDeviceCount = new System.Windows.Forms.Label();
            this.btnMeasure = new System.Windows.Forms.Button();
            this.stopRecordingButton = new System.Windows.Forms.Button();
            this.liveMeasurementTimer = new System.Windows.Forms.Timer(this.components);
            this.rtbSteps = new System.Windows.Forms.RichTextBox();
            this.rtbData = new System.Windows.Forms.RichTextBox();
            this.cbxChannel = new System.Windows.Forms.ComboBox();
            this.recordButton = new System.Windows.Forms.Button();
            this.sampleNameTextBox = new System.Windows.Forms.TextBox();
            this.refreshFileNamesButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.recordingStatusBox = new System.Windows.Forms.RichTextBox();
            this.recordingStatusLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.sampleIdNumberBox = new System.Windows.Forms.NumericUpDown();
            this.isSampleNameAutoIncremented = new System.Windows.Forms.CheckBox();
            this.stopAllButton = new System.Windows.Forms.Button();
            this.timeSelectBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.recordingTimer = new System.Windows.Forms.Timer(this.components);
            this.openContainingFolderButton = new System.Windows.Forms.Button();
            this.mtbFilesTable = new System.Windows.Forms.DataGridView();
            this.FileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RunMatlabAnalysisButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.IsAnalyzed = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PS_X = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PS_Y = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PS_Z = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.matlabProgressBar = new System.Windows.Forms.ProgressBar();
            this.analysisProgressLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.sampleIdNumberBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mtbFilesTable)).BeginInit();
            this.SuspendLayout();
            // 
            // btnScan
            // 
            this.btnScan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnScan.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnScan.Location = new System.Drawing.Point(6, 11);
            this.btnScan.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnScan.Name = "btnScan";
            this.btnScan.Size = new System.Drawing.Size(269, 76);
            this.btnScan.TabIndex = 0;
            this.btnScan.Text = "Scan";
            this.btnScan.UseVisualStyleBackColor = true;
            this.btnScan.Click += new System.EventHandler(this.btnScan_Click);
            // 
            // btnEnable
            // 
            this.btnEnable.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEnable.Enabled = false;
            this.btnEnable.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnable.Location = new System.Drawing.Point(9, 174);
            this.btnEnable.Name = "btnEnable";
            this.btnEnable.Size = new System.Drawing.Size(269, 74);
            this.btnEnable.TabIndex = 3;
            this.btnEnable.Text = "Enable Radio";
            this.btnEnable.UseVisualStyleBackColor = true;
            this.btnEnable.Click += new System.EventHandler(this.btnEnable_Click);
            // 
            // lblChannel
            // 
            this.lblChannel.AutoSize = true;
            this.lblChannel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChannel.Location = new System.Drawing.Point(9, 146);
            this.lblChannel.Name = "lblChannel";
            this.lblChannel.Size = new System.Drawing.Size(64, 17);
            this.lblChannel.TabIndex = 4;
            this.lblChannel.Text = "Channel:";
            // 
            // cbxStations
            // 
            this.cbxStations.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxStations.FormattingEnabled = true;
            this.cbxStations.Location = new System.Drawing.Point(9, 107);
            this.cbxStations.Name = "cbxStations";
            this.cbxStations.Size = new System.Drawing.Size(181, 24);
            this.cbxStations.TabIndex = 5;
            // 
            // lblDeviceCount
            // 
            this.lblDeviceCount.AutoSize = true;
            this.lblDeviceCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDeviceCount.Location = new System.Drawing.Point(149, 146);
            this.lblDeviceCount.Name = "lblDeviceCount";
            this.lblDeviceCount.Size = new System.Drawing.Size(135, 17);
            this.lblDeviceCount.TabIndex = 6;
            this.lblDeviceCount.Text = "MTw\'s Connected: 0";
            // 
            // btnMeasure
            // 
            this.btnMeasure.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMeasure.Enabled = false;
            this.btnMeasure.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMeasure.Location = new System.Drawing.Point(9, 262);
            this.btnMeasure.Name = "btnMeasure";
            this.btnMeasure.Size = new System.Drawing.Size(269, 73);
            this.btnMeasure.TabIndex = 8;
            this.btnMeasure.Text = "Measure";
            this.btnMeasure.UseVisualStyleBackColor = true;
            this.btnMeasure.Click += new System.EventHandler(this.btnMeasure_Click);
            // 
            // stopRecordingButton
            // 
            this.stopRecordingButton.BackColor = System.Drawing.Color.Firebrick;
            this.stopRecordingButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.stopRecordingButton.Enabled = false;
            this.stopRecordingButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stopRecordingButton.Location = new System.Drawing.Point(475, 191);
            this.stopRecordingButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.stopRecordingButton.Name = "stopRecordingButton";
            this.stopRecordingButton.Size = new System.Drawing.Size(170, 104);
            this.stopRecordingButton.TabIndex = 9;
            this.stopRecordingButton.Text = "Stop Recording";
            this.stopRecordingButton.UseVisualStyleBackColor = false;
            this.stopRecordingButton.Click += new System.EventHandler(this.stopRecordingButton_Click);
            // 
            // liveMeasurementTimer
            // 
            this.liveMeasurementTimer.Tick += new System.EventHandler(this.liveMeasurementTimer_Tick);
            // 
            // rtbSteps
            // 
            this.rtbSteps.Location = new System.Drawing.Point(9, 441);
            this.rtbSteps.Name = "rtbSteps";
            this.rtbSteps.ReadOnly = true;
            this.rtbSteps.Size = new System.Drawing.Size(266, 120);
            this.rtbSteps.TabIndex = 10;
            this.rtbSteps.Text = "";
            this.rtbSteps.TextChanged += new System.EventHandler(this.rtbSteps_TextChanged);
            // 
            // rtbData
            // 
            this.rtbData.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbData.Location = new System.Drawing.Point(292, 441);
            this.rtbData.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.rtbData.Name = "rtbData";
            this.rtbData.Size = new System.Drawing.Size(355, 120);
            this.rtbData.TabIndex = 11;
            this.rtbData.Text = "";
            // 
            // cbxChannel
            // 
            this.cbxChannel.DisplayMember = "19";
            this.cbxChannel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxChannel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxChannel.FormattingEnabled = true;
            this.cbxChannel.Items.AddRange(new object[] {
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25"});
            this.cbxChannel.Location = new System.Drawing.Point(72, 144);
            this.cbxChannel.Name = "cbxChannel";
            this.cbxChannel.Size = new System.Drawing.Size(73, 21);
            this.cbxChannel.TabIndex = 12;
            // 
            // recordButton
            // 
            this.recordButton.BackColor = System.Drawing.Color.Green;
            this.recordButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.recordButton.Enabled = false;
            this.recordButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.recordButton.Location = new System.Drawing.Point(292, 191);
            this.recordButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.recordButton.Name = "recordButton";
            this.recordButton.Size = new System.Drawing.Size(179, 104);
            this.recordButton.TabIndex = 13;
            this.recordButton.Text = "Record";
            this.recordButton.UseVisualStyleBackColor = false;
            this.recordButton.Click += new System.EventHandler(this.recordButton_Click);
            // 
            // sampleNameTextBox
            // 
            this.sampleNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sampleNameTextBox.Location = new System.Drawing.Point(493, 27);
            this.sampleNameTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.sampleNameTextBox.Name = "sampleNameTextBox";
            this.sampleNameTextBox.Size = new System.Drawing.Size(121, 29);
            this.sampleNameTextBox.TabIndex = 21;
            this.sampleNameTextBox.Text = "Example";
            this.sampleNameTextBox.TextChanged += new System.EventHandler(this.textBoxFilename_TextChanged);
            // 
            // refreshFileNamesButton
            // 
            this.refreshFileNamesButton.BackColor = System.Drawing.Color.CornflowerBlue;
            this.refreshFileNamesButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.refreshFileNamesButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refreshFileNamesButton.Location = new System.Drawing.Point(670, 8);
            this.refreshFileNamesButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.refreshFileNamesButton.Name = "refreshFileNamesButton";
            this.refreshFileNamesButton.Size = new System.Drawing.Size(245, 77);
            this.refreshFileNamesButton.TabIndex = 24;
            this.refreshFileNamesButton.Text = "Refresh Files";
            this.refreshFileNamesButton.UseVisualStyleBackColor = false;
            this.refreshFileNamesButton.Click += new System.EventHandler(this.refreshFileNamesButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(287, 405);
            this.label1.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 29);
            this.label1.TabIndex = 25;
            this.label1.Text = "Live Data";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 405);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(186, 29);
            this.label2.TabIndex = 26;
            this.label2.Text = "Xsens Message";
            // 
            // recordingStatusBox
            // 
            this.recordingStatusBox.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.recordingStatusBox.Location = new System.Drawing.Point(444, 324);
            this.recordingStatusBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.recordingStatusBox.Name = "recordingStatusBox";
            this.recordingStatusBox.Size = new System.Drawing.Size(201, 68);
            this.recordingStatusBox.TabIndex = 27;
            this.recordingStatusBox.Text = "Recorded for 5 seconds";
            // 
            // recordingStatusLabel
            // 
            this.recordingStatusLabel.AutoSize = true;
            this.recordingStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.recordingStatusLabel.Location = new System.Drawing.Point(288, 342);
            this.recordingStatusLabel.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.recordingStatusLabel.Name = "recordingStatusLabel";
            this.recordingStatusLabel.Size = new System.Drawing.Size(153, 24);
            this.recordingStatusLabel.TabIndex = 28;
            this.recordingStatusLabel.Text = "Recording Status";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(354, 26);
            this.label4.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(135, 24);
            this.label4.TabIndex = 29;
            this.label4.Text = "Sample Name:";
            // 
            // sampleIdNumberBox
            // 
            this.sampleIdNumberBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sampleIdNumberBox.Location = new System.Drawing.Point(493, 65);
            this.sampleIdNumberBox.Margin = new System.Windows.Forms.Padding(1);
            this.sampleIdNumberBox.Name = "sampleIdNumberBox";
            this.sampleIdNumberBox.Size = new System.Drawing.Size(80, 29);
            this.sampleIdNumberBox.TabIndex = 30;
            // 
            // isSampleNameAutoIncremented
            // 
            this.isSampleNameAutoIncremented.AutoSize = true;
            this.isSampleNameAutoIncremented.Checked = true;
            this.isSampleNameAutoIncremented.CheckState = System.Windows.Forms.CheckState.Checked;
            this.isSampleNameAutoIncremented.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.isSampleNameAutoIncremented.Location = new System.Drawing.Point(492, 103);
            this.isSampleNameAutoIncremented.Margin = new System.Windows.Forms.Padding(1);
            this.isSampleNameAutoIncremented.Name = "isSampleNameAutoIncremented";
            this.isSampleNameAutoIncremented.Size = new System.Drawing.Size(122, 21);
            this.isSampleNameAutoIncremented.TabIndex = 31;
            this.isSampleNameAutoIncremented.Text = "Auto Increment";
            this.isSampleNameAutoIncremented.UseVisualStyleBackColor = true;
            this.isSampleNameAutoIncremented.CheckedChanged += new System.EventHandler(this.isAutoIncremented_CheckedChanged);
            // 
            // stopAllButton
            // 
            this.stopAllButton.BackColor = System.Drawing.Color.Orange;
            this.stopAllButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.stopAllButton.Enabled = false;
            this.stopAllButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stopAllButton.Location = new System.Drawing.Point(9, 342);
            this.stopAllButton.Name = "stopAllButton";
            this.stopAllButton.Size = new System.Drawing.Size(269, 60);
            this.stopAllButton.TabIndex = 32;
            this.stopAllButton.Text = "Disconnect";
            this.stopAllButton.UseVisualStyleBackColor = false;
            this.stopAllButton.Click += new System.EventHandler(this.stopAllButton_Click);
            // 
            // timeSelectBox
            // 
            this.timeSelectBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.timeSelectBox.FormattingEnabled = true;
            this.timeSelectBox.Items.AddRange(new object[] {
            "1",
            "5",
            "15",
            "30",
            "45",
            "60",
            "Unlimited"});
            this.timeSelectBox.Location = new System.Drawing.Point(493, 129);
            this.timeSelectBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.timeSelectBox.Name = "timeSelectBox";
            this.timeSelectBox.Size = new System.Drawing.Size(120, 28);
            this.timeSelectBox.TabIndex = 33;
            this.timeSelectBox.SelectedIndexChanged += new System.EventHandler(this.timeSelectBox_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label3.Location = new System.Drawing.Point(317, 131);
            this.label3.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(172, 24);
            this.label3.TabIndex = 34;
            this.label3.Text = "Recording Time (s)";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // recordingTimer
            // 
            this.recordingTimer.Tick += new System.EventHandler(this.recordingTimer_Tick_1);
            // 
            // openContainingFolderButton
            // 
            this.openContainingFolderButton.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.openContainingFolderButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.openContainingFolderButton.Location = new System.Drawing.Point(919, 11);
            this.openContainingFolderButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.openContainingFolderButton.Name = "openContainingFolderButton";
            this.openContainingFolderButton.Size = new System.Drawing.Size(248, 73);
            this.openContainingFolderButton.TabIndex = 53;
            this.openContainingFolderButton.Text = "Open Containing Folder";
            this.openContainingFolderButton.UseVisualStyleBackColor = false;
            this.openContainingFolderButton.Click += new System.EventHandler(this.button10_Click);
            // 
            // mtbFilesTable
            // 
            this.mtbFilesTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mtbFilesTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FileName,
            this.FileSize,
            this.RunMatlabAnalysisButton,
            this.IsAnalyzed,
            this.PS_X,
            this.PS_Y,
            this.PS_Z});
            this.mtbFilesTable.Location = new System.Drawing.Point(670, 89);
            this.mtbFilesTable.Name = "mtbFilesTable";
            this.mtbFilesTable.RowHeadersWidth = 51;
            this.mtbFilesTable.Size = new System.Drawing.Size(897, 471);
            this.mtbFilesTable.TabIndex = 54;
            this.mtbFilesTable.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // FileName
            // 
            this.FileName.DataPropertyName = "FileName";
            this.FileName.HeaderText = "File Name";
            this.FileName.MinimumWidth = 6;
            this.FileName.Name = "FileName";
            this.FileName.Width = 150;
            // 
            // FileSize
            // 
            this.FileSize.DataPropertyName = "FileSize";
            this.FileSize.HeaderText = "File Size";
            this.FileSize.MinimumWidth = 6;
            this.FileSize.Name = "FileSize";
            this.FileSize.Width = 75;
            // 
            // RunMatlabAnalysisButton
            // 
            this.RunMatlabAnalysisButton.DataPropertyName = "OnClick";
            this.RunMatlabAnalysisButton.HeaderText = "Run Matlab Script";
            this.RunMatlabAnalysisButton.MinimumWidth = 6;
            this.RunMatlabAnalysisButton.Name = "RunMatlabAnalysisButton";
            this.RunMatlabAnalysisButton.Width = 150;
            // 
            // IsAnalyzed
            // 
            this.IsAnalyzed.DataPropertyName = "IsAnalyzed";
            this.IsAnalyzed.HeaderText = "Analyzed?";
            this.IsAnalyzed.MinimumWidth = 6;
            this.IsAnalyzed.Name = "IsAnalyzed";
            this.IsAnalyzed.ReadOnly = true;
            this.IsAnalyzed.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IsAnalyzed.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.IsAnalyzed.Width = 70;
            // 
            // PS_X
            // 
            this.PS_X.HeaderText = "PS_X";
            this.PS_X.MinimumWidth = 6;
            this.PS_X.Name = "PS_X";
            this.PS_X.Width = 125;
            // 
            // PS_Y
            // 
            this.PS_Y.HeaderText = "PS_Y";
            this.PS_Y.MinimumWidth = 6;
            this.PS_Y.Name = "PS_Y";
            this.PS_Y.Width = 125;
            // 
            // PS_Z
            // 
            this.PS_Z.HeaderText = "PS_Z";
            this.PS_Z.MinimumWidth = 6;
            this.PS_Z.Name = "PS_Z";
            this.PS_Z.Width = 125;
            // 
            // matlabProgressBar
            // 
            this.matlabProgressBar.Location = new System.Drawing.Point(1365, 567);
            this.matlabProgressBar.Name = "matlabProgressBar";
            this.matlabProgressBar.Size = new System.Drawing.Size(202, 23);
            this.matlabProgressBar.TabIndex = 55;
            // 
            // analysisProgressLabel
            // 
            this.analysisProgressLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.analysisProgressLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.analysisProgressLabel.Location = new System.Drawing.Point(1152, 563);
            this.analysisProgressLabel.Name = "analysisProgressLabel";
            this.analysisProgressLabel.Size = new System.Drawing.Size(198, 30);
            this.analysisProgressLabel.TabIndex = 56;
            this.analysisProgressLabel.Text = "Matlab Analysis Progress";
            this.analysisProgressLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1579, 602);
            this.Controls.Add(this.analysisProgressLabel);
            this.Controls.Add(this.matlabProgressBar);
            this.Controls.Add(this.mtbFilesTable);
            this.Controls.Add(this.openContainingFolderButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.timeSelectBox);
            this.Controls.Add(this.stopAllButton);
            this.Controls.Add(this.isSampleNameAutoIncremented);
            this.Controls.Add(this.sampleIdNumberBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.recordingStatusLabel);
            this.Controls.Add(this.recordingStatusBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.refreshFileNamesButton);
            this.Controls.Add(this.sampleNameTextBox);
            this.Controls.Add(this.recordButton);
            this.Controls.Add(this.cbxChannel);
            this.Controls.Add(this.rtbData);
            this.Controls.Add(this.rtbSteps);
            this.Controls.Add(this.stopRecordingButton);
            this.Controls.Add(this.btnMeasure);
            this.Controls.Add(this.lblDeviceCount);
            this.Controls.Add(this.cbxStations);
            this.Controls.Add(this.lblChannel);
            this.Controls.Add(this.btnEnable);
            this.Controls.Add(this.btnScan);
            this.Name = "Form1";
            this.Text = " ";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.sampleIdNumberBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mtbFilesTable)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

		private System.Windows.Forms.Button btnScan;
        private System.Windows.Forms.Button btnEnable;
        private System.Windows.Forms.Label lblChannel;
        private System.Windows.Forms.ComboBox cbxStations;
		private System.Windows.Forms.Label lblDeviceCount;
        private System.Windows.Forms.Button btnMeasure;
        private System.Windows.Forms.Button stopRecordingButton;
        private System.Windows.Forms.Timer liveMeasurementTimer;
        private System.Windows.Forms.RichTextBox rtbSteps;
        private System.Windows.Forms.RichTextBox rtbData;
		private System.Windows.Forms.ComboBox cbxChannel;
		private System.Windows.Forms.Button recordButton;
		private System.Windows.Forms.TextBox sampleNameTextBox;
        private System.Windows.Forms.Button refreshFileNamesButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox recordingStatusBox;
        private System.Windows.Forms.Label recordingStatusLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown sampleIdNumberBox;
        private System.Windows.Forms.CheckBox isSampleNameAutoIncremented;
        private System.Windows.Forms.Button stopAllButton;
        private System.Windows.Forms.ComboBox timeSelectBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Timer recordingTimer;
        private System.Windows.Forms.Button openContainingFolderButton;
        private System.Windows.Forms.DataGridView mtbFilesTable;
        private System.Windows.Forms.ProgressBar matlabProgressBar;
        private System.Windows.Forms.Label analysisProgressLabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileSize;
        private System.Windows.Forms.DataGridViewButtonColumn RunMatlabAnalysisButton;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsAnalyzed;
        private System.Windows.Forms.DataGridViewTextBoxColumn PS_X;
        private System.Windows.Forms.DataGridViewTextBoxColumn PS_Y;
        private System.Windows.Forms.DataGridViewTextBoxColumn PS_Z;
    }
}

